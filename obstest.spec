Name:         obstest
Summary:      Console based Tetris clone
License:      GPL
URL:          http://www.hackl.dhs.org/ctris/ 
Group:        Amusements/Games/Action/Arcade
Version:      1.0
Release:      1
Source:       %{name}-%{version}.tar.gz
BuildRequires: cmake gcc-c++
BuildRoot:    %{_tmppath}/%{name}-%{version}-build

%description
An ASCII version of the well known game Tetris.

%prep
%setup

%build
cmake -DCMAKE_SKIP_RPATH=ON -DCMAKE_INSTALL_PREFIX=/usr/local .
make

%install
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT;

%files
%defattr (-,root,root)
/usr/local/bin/hello

%changelog
